#pragma once

#include <string>
#include <vector>
#include "Misc.h"

#ifndef interface
#define interface struct
#endif

#define VF_PC 1
#define VF_PN 2
#define VF_PNT 3

// 모든 버텍스의 인터페이스
interface IVertex
{
	virtual void PutVertex() const = 0;
};

// 좌표와 색상만을 가지는 버텍스
struct PCVertex : public IVertex
{
	Vector3 position;
	Vector4 color;
	virtual void PutVertex() const
	{
		glColor4fv(color.GetPointer());
		glVertex3fv(position.GetPointer());
	}
};

// 좌표, 법선벡터만을 가지는 버텍스
struct PNVertex : public IVertex
{
	Vector3 position;
	Vector3 normal;
	virtual void PutVertex() const
	{
		glNormal3fv(normal.GetPointer());
		glVertex3fv(position.GetPointer());
	}
};

// 좌표, 법선벡터, 텍스처 매핑 좌표만을 가지는 버텍스
struct PNTVertex : public IVertex
{
	Vector3 position;
	Vector3 normal;
	Vector2 uv;
	virtual void PutVertex() const
	{
		glTexCoord2fv(uv.GetPointer());
		glNormal3fv(normal.GetPointer());
		glVertex3fv(position.GetPointer());
	}
};

// OpenGL로 렌더링할 수 있는 객체
class RenderObject
{
	friend class OpenGL;

public:
	// 렌더링 객체를 이루는 일부 객체
	struct Subset
	{
		unsigned int m_nRenderingMethod;	// OpenGL 그리기 방식
		unsigned int m_nVertexFormat;
		std::vector<IVertex *> m_vecVertex;	// 버텍스 버퍼

		Subset(unsigned int renderingMethod, unsigned int vertexFormat);
		~Subset();
		void Render();
		void Clear();
	};

private:
	RenderObject(const std::wstring &name);
	~RenderObject();

	void Render();

public:
	std::wstring GetName() const;

	int GetNumSubset();
	Subset *GetSubset(int index);
	void DeleteSubset(unsigned int index);

	bool BeginNewSubset(int renderingMethod, unsigned int vertexFormat);
	void EndNewSubset();

	void AddVertex(const Vector3 &position, const Vector4 &color);
	void AddVertex(const Vector3 &position, const Vector3 &normal);
	void AddVertex(const Vector3 &position, const Vector3 &normal, const Vector2 &uv);
	void AddVertex(float x, float y, float z, float r, float g, float b, float a);
	void AddVertex(float x, float y, float z, float nx, float ny, float nz);
	void AddVertex(float x, float y, float z, float nx, float ny, float nz, float u, float v);

private:
	std::wstring m_strName;	// 오브젝트 이름
	std::vector<Subset *> m_vecSubset;	// 서브셋 목록
	bool m_bSettingSubset;	// 현재 새로운 서브셋을 만들고 있는가
	std::vector<Subset *>::iterator m_itrCurrentSubset;	// 현재 만들고 있는 서브셋 반복자
};

