#include "StdAfx.h"
#include "ArcBallCamera.h"

ArcBallCamera::ArcBallCamera(float distanceFromTarget)
	: m_nCameraMode(CAMERA_PERSPECTIVE)
	, m_fDistanceFromTarget(distanceFromTarget)
	, m_vUp(0.0f, 1.0f, 0.0f)
	, m_fAngleX(0.0f)
	, m_fAngleY(0.0f)
	, m_fAngleZ(0.0f)
	, m_bFreezeRevolve(false)
{
}
ArcBallCamera::~ArcBallCamera(void)
{
}

Vector3 ArcBallCamera::GetPosition()
{
	return m_ptPosition;
}

void ArcBallCamera::SetCameraMode(int cameraMode)
{
	m_nCameraMode = cameraMode;
}

void ArcBallCamera::FreezeRevolve(bool flag)
{
	m_bFreezeRevolve = flag;
}

void ArcBallCamera::SetDistanceFromTarget(float distance)
{
	m_fDistanceFromTarget = distance;
}

void ArcBallCamera::SetProjection(int width, int height)
{
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	switch(m_nCameraMode)
	{
	case CAMERA_PERSPECTIVE:
		gluPerspective(45.0f, (float)width / (float)height, 0.1, 10000.0);
		break;
	case CAMERA_ORTHOGONAL:
		glOrtho(-(double)width / 50.0, (double)width / 50.0, -(double)height / 50.0, (double)height / 50.0, -1000.0, 1000.0);
		break;
	}
}

void ArcBallCamera::RevolveByAxisX(float angle)
{
	if(m_bFreezeRevolve)
		return;

	m_fAngleX += angle;
}
void ArcBallCamera::RevolveByAxisY(float angle)
{
	if(m_bFreezeRevolve)
		return;

	m_fAngleY += angle;
}
void ArcBallCamera::RevolveByAxisZ(float angle)
{
	if(m_bFreezeRevolve)
		return;

	m_fAngleZ += angle;
}

void ArcBallCamera::SetRevolveByAxisX(float angle)
{
	m_fAngleX = angle;
}
void ArcBallCamera::SetRevolveByAxisY(float angle)
{
	m_fAngleY = angle;
}
void ArcBallCamera::SetRevolveByAxisZ(float angle)
{
	m_fAngleZ = angle;
}

void ArcBallCamera::Update()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	this->UpdatePosition();
}

void ArcBallCamera::UpdatePosition()
{
	// 공전 행렬 설정
	Matrix matTrans, matRotateX, matRotateY, matRotateZ, mat;
	matTrans.MakeTranslation(0.0f, 0.0f, m_fDistanceFromTarget);
	matRotateX.MakeRotationX(m_fAngleX);
	matRotateY.MakeRotationY(m_fAngleY);
	matRotateZ.MakeRotationZ(m_fAngleZ);
	mat = matTrans * matRotateX * matRotateY * matRotateZ;

	// 현재 카메라 위치 갱신
	m_ptPosition.Set(mat.m[12], mat.m[13], mat.m[14]);

	// up벡터도 돌려주기

	// 공전
	gluLookAt(mat.m[12], mat.m[13], mat.m[14]
		, m_ptTarget.x, m_ptTarget.y, m_ptTarget.z
		, m_vUp.x, m_vUp.y, m_vUp.z);
}
