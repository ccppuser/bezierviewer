
// BezierViewer.h : main header file for the BezierViewer application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols
#include "OpenGL.h"
#include "BezierRenderer.h"


// CBezierViewerApp:
// See BezierViewer.cpp for the implementation of this class
//

class CBezierViewerApp : public CWinApp
{
public:
	CBezierViewerApp();


// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual BOOL OnIdle(LONG lCount); // return TRUE if more idle processing

// Implementation

public:
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()

public:
	OpenGL *m_pOpenGL;
	BezierRenderer *m_pBezierRenderer;
	clock_t m_tLastTime;
	float m_fTimeDelta;
};

extern CBezierViewerApp theApp;
