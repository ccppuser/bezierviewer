#include "StdAfx.h"
#include "OpenGL.h"

OpenGL::OpenGL()
	: m_hWnd(0)
	, m_hDC(0)
	, m_hRC(0)
{
}
OpenGL::~OpenGL(void)
{
	this->Cleanup();
}

bool OpenGL::Initialize(HWND targetHandle)
{
	m_hWnd = targetHandle;
	m_hDC = GetDC(m_hWnd);
	if(!m_hDC)
		return false;

	if(!this->SetupPixelFormat(m_hDC))
		return false;

	m_hRC = wglCreateContext(m_hDC);
	if(!m_hRC)
		return false;

	if(!wglMakeCurrent(m_hDC, m_hRC))
		return false;

	RECT rect;
	GetClientRect(targetHandle, &rect);
	int width = rect.right;
	int height = rect.bottom;

	this->InitializeEnvironment(width, height);

	return true;
}

void OpenGL::Cleanup()
{
	if(m_hDC)
	{
		ReleaseDC(m_hWnd, m_hDC);
		m_hDC = 0;
	}
	if(m_hRC)
	{
		wglDeleteContext(m_hRC);
		m_hRC = 0;
	}
	for(std::vector<RenderObject *>::iterator itr = m_vecRObject.begin(); itr != m_vecRObject.end(); ++itr)
	{
		if((*itr))
		{
			delete (*itr);
			(*itr) = NULL;
		}
	}
	m_vecRObject.clear();
}

void OpenGL::Render()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_camera.Update();

	glMatrixMode(GL_MODELVIEW);

	glPointSize(5.0f);

	for(std::vector<RenderObject *>::const_iterator itr = m_vecRObject.begin(); itr != m_vecRObject.end(); ++itr)
	{
		glPushMatrix();

		(*itr)->Render();

		glPopMatrix();
	}

	SwapBuffers(m_hDC);
}

ArcBallCamera *OpenGL::GetArcBallCamera()
{
	return &m_camera;
}

RenderObject *OpenGL::CreateRenderObject(std::wstring objectName)
{
	RenderObject *pNewRObject = new RenderObject(objectName);
	m_vecRObject.push_back(pNewRObject);
	return pNewRObject;
}

void OpenGL::DeleteRenderObject(std::wstring objectName)
{
	for(std::vector<RenderObject *>::iterator itr = m_vecRObject.begin(); itr != m_vecRObject.end(); ++itr)
	{
		if((*itr)->GetName() == objectName)
		{
			delete (*itr);
			(*itr) = NULL;
			m_vecRObject.erase(itr);
			break;
		}
	}
}

void OpenGL::SetTopView()
{
	m_camera.SetRevolveByAxisX(3.14f * 0.4999f);
	m_camera.SetRevolveByAxisY(0.0f);
	m_camera.FreezeRevolve(true);
}
void OpenGL::SetFrontView()
{
	m_camera.SetRevolveByAxisX(0.0f);
	m_camera.SetRevolveByAxisY(0.0f);
	m_camera.FreezeRevolve(true);
}
void OpenGL::SetPerspectiveView()
{
	m_camera.SetRevolveByAxisX(3.14f * 0.25f);
	m_camera.SetRevolveByAxisY(3.14f * 0.25f);
	m_camera.FreezeRevolve(false);
}

Vector3 OpenGL::PickZeroHeightCoord(int windowX, int windowY)
{
	if(!GetCapture())
		return Vector3();

	Ray pickingRay;
	int viewport[4];
	double matProj[16];
	double matView[16];
	double wx, wy, wz;
	
	// 현재 변환 행렬 구함
	glGetIntegerv(GL_VIEWPORT, viewport);
	glGetDoublev(GL_MODELVIEW_MATRIX, matView);
	glGetDoublev(GL_PROJECTION_MATRIX, matProj);

	// 화면 안으로 발사하는 광선의 시작 점 구함
	glPushMatrix();
	gluUnProject((double)windowX, (double)(viewport[3] - windowY), 0.0, matView, matProj, viewport, &wx, &wy, &wz);
	pickingRay.origin.Set((float)wx, (float)wy, (float)wz);
	glPopMatrix();

	// 화면 안으로 발사하는 광선의 방향 구함
	glPushMatrix();
	gluUnProject((double)windowX, (double)(viewport[3] - windowY), 1.0, matView, matProj, viewport, &wx, &wy, &wz);
	pickingRay.direction = Vector3((float)wx, (float)wy, (float)wz) - pickingRay.origin;
	glPopMatrix();

	// 광선과 평면의 교차점 구함

	return Vector3();
}

bool OpenGL::SetupPixelFormat(HDC hDC)
{
	PIXELFORMATDESCRIPTOR pfd;
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.dwLayerMask = PFD_MAIN_PLANE;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 24;
	pfd.cDepthBits = 16;
	pfd.cAccumBits = 0;
	pfd.cStencilBits = 0;

	int pixelFormat = ChoosePixelFormat(hDC, &pfd);
	if(pixelFormat == 0)
		return false;

	if(!SetPixelFormat(hDC, pixelFormat, &pfd))
		return false;

	return true;
}

void OpenGL::InitializeEnvironment(int width, int height)
{
	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);	// 깊이버퍼를 이용한 은면제거 활성화
	glEnable(GL_CULL_FACE);	// 뒷면 제거 활성화
	glCullFace(GL_BACK);
	glFrontFace(GL_CW);

	// 방향성 광원 설정
	float position[] = {1.0f, -1.0f, -1.0f, 0.0f};	// 방향성 광원이므로 w = 0.0f;
	glLightfv(GL_LIGHT0, GL_POSITION, position);
	float ambient[] = {0.2f, 0.0f, 0.0f, 1.0f};
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
	float diffuse[] = {0.7f, 0.0f, 0.0f, 1.0f};
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
	float specular[] = {1.0f, 0.0f, 0.0f, 1.0f};
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular);

	//Vector3 lightDirection(1.0f, -1.0f, -1.0f);
	//lightDirection.Normalize();
	/*glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, lightDirection.GetPointer());
	glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 45.0f);
	glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, 8.0f);*/

	// 광원 설치
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	// 투영 설정
	m_camera.SetProjection(width, height);
}
