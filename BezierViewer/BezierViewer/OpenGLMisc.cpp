#include "stdafx.h"
#include "OpenGLMisc.h"

RenderObject::Subset::Subset(unsigned int renderingMethod, unsigned int vertexFormat)
	: m_nRenderingMethod(renderingMethod)
	, m_nVertexFormat(vertexFormat)
{
}
RenderObject::Subset::~Subset()
{
	this->Clear();
}

void RenderObject::Subset::Render()
{
	if(m_nVertexFormat == VF_PC)
		glDisable(GL_LIGHTING);

	glBegin(m_nRenderingMethod);
	{
		for(std::vector<IVertex *>::iterator itr = m_vecVertex.begin(); itr != m_vecVertex.end(); ++itr)
		{
			(*itr)->PutVertex();
		}
	}
	glEnd();

	if(m_nVertexFormat == VF_PC)
		glEnable(GL_LIGHTING);
}

void RenderObject::Subset::Clear()
{
	for(std::vector<IVertex *>::iterator itr = m_vecVertex.begin(); itr != m_vecVertex.end(); ++itr)
	{
		if((*itr))
		{
			delete (*itr);
			(*itr) = NULL;
		}
	}
	m_vecVertex.clear();
}

RenderObject::RenderObject(const std::wstring &name)
	: m_strName(name)
	, m_bSettingSubset(false)
{
}
RenderObject::~RenderObject()
{
	for(std::vector<Subset *>::iterator itr = m_vecSubset.begin(); itr != m_vecSubset.end(); ++itr)
	{
		if((*itr))
		{
			delete (*itr);
			(*itr) = NULL;
		}
	}
	m_vecSubset.clear();
}

void RenderObject::Render()
{
	for(std::vector<Subset *>::iterator itrSubset = m_vecSubset.begin(); itrSubset != m_vecSubset.end(); ++itrSubset)
	{
		(*itrSubset)->Render();
	}
}

std::wstring RenderObject::GetName() const
{
	return m_strName;
}

int RenderObject::GetNumSubset()
{
	return (int)m_vecSubset.size();
}

void RenderObject::DeleteSubset(unsigned int index)
{
	if(index >= m_vecSubset.size())
		return;

	std::vector<Subset *>::iterator itr = m_vecSubset.begin() + index;
	if((*itr))
	{
		delete (*itr);
		(*itr) = NULL;
	}
	m_vecSubset.erase(itr);
}

RenderObject::Subset *RenderObject::GetSubset(int index)
{
	return m_vecSubset[index];
}

bool RenderObject::BeginNewSubset(int renderingMethod, unsigned int vertexFormat)
{
	if(m_bSettingSubset)
		return false;

	m_bSettingSubset = true;
	Subset *pNewSubset = new Subset(renderingMethod, vertexFormat);
	m_vecSubset.push_back(pNewSubset);
	m_itrCurrentSubset = m_vecSubset.end() - 1;

	return true;
}

void RenderObject::EndNewSubset()
{
	m_bSettingSubset = false;
}

void RenderObject::AddVertex(const Vector3 &position, const Vector4 &color)
{
	if(!m_bSettingSubset)
		return;

	PCVertex *pNewVertex = new PCVertex;
	pNewVertex->position = position;
	pNewVertex->color = color;
	(*m_itrCurrentSubset)->m_vecVertex.push_back(pNewVertex);
}
void RenderObject::AddVertex(const Vector3 &position, const Vector3 &normal)
{
	if(!m_bSettingSubset)
		return;

	PNVertex *pNewVertex = new PNVertex;
	pNewVertex->position = position;
	pNewVertex->normal = normal;
	(*m_itrCurrentSubset)->m_vecVertex.push_back(pNewVertex);
}
void RenderObject::AddVertex(const Vector3 &position, const Vector3 &normal, const Vector2 &uv)
{
	if(!m_bSettingSubset)
		return;

	PNTVertex *pNewVertex = new PNTVertex;
	pNewVertex->position = position;
	pNewVertex->normal = normal;
	pNewVertex->uv = uv;
	(*m_itrCurrentSubset)->m_vecVertex.push_back(pNewVertex);
}
void RenderObject::AddVertex(float x, float y, float z, float r, float g, float b, float a)
{
	if(!m_bSettingSubset)
		return;

	PCVertex *pNewVertex = new PCVertex;
	pNewVertex->position.Set(x, y, z);
	pNewVertex->color.Set(r, g, b, a);
	(*m_itrCurrentSubset)->m_vecVertex.push_back(pNewVertex);
}
void RenderObject::AddVertex(float x, float y, float z, float nx, float ny, float nz)
{
	if(!m_bSettingSubset)
		return;

	PNVertex *pNewVertex = new PNVertex;
	pNewVertex->position.Set(x, y, z);
	pNewVertex->normal.Set(nx, ny, nz);
	(*m_itrCurrentSubset)->m_vecVertex.push_back(pNewVertex);
}
void RenderObject::AddVertex(float x, float y, float z, float nx, float ny, float nz, float u, float v)
{
	if(!m_bSettingSubset)
		return;

	PNTVertex *pNewVertex = new PNTVertex;
	pNewVertex->position.Set(x, y, z);
	pNewVertex->normal.Set(nx, ny, nz);
	pNewVertex->uv.Set(u, v);
	(*m_itrCurrentSubset)->m_vecVertex.push_back(pNewVertex);
}
