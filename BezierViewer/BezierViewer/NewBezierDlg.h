#pragma once


// CNewBezierCurveDlg dialog

class CNewBezierCurveDlg : public CDialog
{
	DECLARE_DYNAMIC(CNewBezierCurveDlg)

public:
	CNewBezierCurveDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CNewBezierCurveDlg();

// Dialog Data
	enum { IDD = IDD_NEWBEZIERCURVEDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

private:
	CEdit m_edtName;
	CEdit m_edtControlPointNumber;

public:
	CString m_strName;
	int m_nControlPointNumber;
};


// CNewBezierSurfaceDlg dialog

class CNewBezierSurfaceDlg : public CDialog
{
	DECLARE_DYNAMIC(CNewBezierSurfaceDlg)

public:
	CNewBezierSurfaceDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CNewBezierSurfaceDlg();

// Dialog Data
	enum { IDD = IDD_NEWBEZIERSURFACEDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

private:
	CEdit m_edtName;
	CEdit m_edtUDirControlPointNumber;
	CEdit m_edtVDirControlPointNumber;

public:
	CString m_strName;
	int m_nUDirControlPointNumber;
	int m_nVDirControlPointNumber;
};
