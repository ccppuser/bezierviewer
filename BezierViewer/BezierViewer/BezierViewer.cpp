
// BezierViewer.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "afxwinappex.h"
#include "afxdialogex.h"
#include "BezierViewer.h"
#include "MainFrm.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CBezierViewerApp

BEGIN_MESSAGE_MAP(CBezierViewerApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CBezierViewerApp::OnAppAbout)
END_MESSAGE_MAP()


// CBezierViewerApp construction

CBezierViewerApp::CBezierViewerApp()
{
	// TODO: replace application ID string below with unique ID string; recommended
	// format for string is CompanyName.ProductName.SubProduct.VersionInformation
	//SetAppID(_T("BezierViewer.AppID.NoVersion"));

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	m_tLastTime = clock();
	m_pOpenGL = new OpenGL;
	m_pBezierRenderer = new BezierRenderer;
}

// The one and only CBezierViewerApp object

CBezierViewerApp theApp;


// CBezierViewerApp initialization

BOOL CBezierViewerApp::InitInstance()
{
	CWinApp::InitInstance();


	//EnableTaskbarInteraction(FALSE);

	// AfxInitRichEdit2() is required to use RichEdit control	
	// AfxInitRichEdit2();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));


	// To create the main window, this code creates a new frame window
	// object and then sets it as the application's main window object
	CMainFrame* pFrame = new CMainFrame;
	if (!pFrame)
		return FALSE;
	m_pMainWnd = pFrame;
	// create and load the frame with its resources
	pFrame->LoadFrame(IDR_MAINFRAME,
		WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, NULL,
		NULL);






	// The one and only window has been initialized, so show and update it
	pFrame->ShowWindow(SW_SHOW);
	pFrame->UpdateWindow();
	// call DragAcceptFiles only if there's a suffix
	//  In an SDI app, this should occur after ProcessShellCommand
	return TRUE;
}

int CBezierViewerApp::ExitInstance()
{
	//TODO: handle additional resources you may have added
	if(m_pOpenGL)
	{
		delete m_pOpenGL;
		m_pOpenGL = NULL;
	}
	if(m_pBezierRenderer)
	{
		delete m_pBezierRenderer;
		m_pBezierRenderer = NULL;
	}
	return CWinApp::ExitInstance();
}

BOOL CBezierViewerApp::OnIdle(LONG lCount)
{
	// 프레임간 경과 시간 계산
	clock_t currentTime = clock();
	if(lCount == 0)
	{
		m_fTimeDelta = (float)(currentTime - m_tLastTime) * 0.001f;
	}
	m_tLastTime = currentTime;

	// 프레임 수 누적
	static unsigned int nFrame = 0;
	++nFrame;

	// FPS 출력
	static float fElapsedTime = 0.0f;
	fElapsedTime += m_fTimeDelta;
	if(fElapsedTime >= 1.0f)
	{
		wchar_t strFPS[32];
		swprintf_s(strFPS, 32, L"FPS: %d\n", nFrame);
		OutputDebugString(strFPS);
		fElapsedTime = 0.0f;
		nFrame = 0;
	}

	// OpenGL 렌더링
	if(m_pOpenGL)
	{
		m_pOpenGL->Render();
	}

	return TRUE;
}

// CBezierViewerApp message handlers


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

// App command to run the dialog
void CBezierViewerApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

// CBezierViewerApp message handlers



