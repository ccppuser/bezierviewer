#pragma once

#include <cmath>
#include <cstring>

#define MISC_OPENGL

class Matrix;
class Vector2;
class Vector3;
class Vector4;
class Ray;

// 행렬
class Matrix
{
public:
	Matrix() { this->MakeIdentity(); }

	Matrix operator + (const Matrix &rhs) { Matrix ret; ret.MakeZero(); for(int i = 0; i < 16; ++i) { ret.m[i] = this->m[i] + rhs.m[i]; } return ret; }
	Matrix operator * (const Matrix &rhs) { Matrix ret; ret.MakeZero(); for(int i = 0; i < 4; ++i) { for(int j = 0; j < 4; ++j) { for(int k = 0; k < 4; ++k) { ret.m[4 * i + j] += m[4 * i + k] * rhs.m[4 * k + j]; }}} return ret; }

	void MakeZero() { memset(m, 0, 16 * sizeof(float)); }
	void MakeIdentity() { this->MakeZero(); m[0] = 1.0f; m[5] = 1.0f; m[10] = 1.0f; m[15] = 1.0f; }
#ifdef MISC_OPENGL
	void MakeRotationX(float radian) { this->MakeIdentity(); m[5] = cosf(radian); m[6] = -sinf(radian); m[9] = sinf(radian); m[10] = cosf(radian); }
	void MakeRotationY(float radian) { this->MakeIdentity(); m[0] = cosf(radian); m[2] = sinf(radian); m[8] = -sinf(radian); m[10] = cosf(radian); }
	void MakeRotationZ(float radian) { this->MakeIdentity(); m[0] = cosf(radian); m[1] = -sinf(radian); m[4] = sinf(radian); m[5] = cosf(radian); }
	void MakeTranslation(float x, float y, float z) { this->MakeIdentity(); m[12] = x; m[13] = y; m[14] = z; }
#else
	void MakeRotationX(float radian) { this->MakeIdentity(); m[5] = cosf(radian); m[6] = sinf(radian); m[9] = -sinf(radian); m[10] = cosf(radian); }
	void MakeRotationY(float radian) { this->MakeIdentity(); m[0] = cosf(radian); m[2] = -sinf(radian); m[8] = sinf(radian); m[10] = cosf(radian); }
	void MakeRotationZ(float radian) { this->MakeIdentity(); m[0] = cosf(radian); m[1] = sinf(radian); m[4] = -sinf(radian); m[5] = cosf(radian); }
#endif

public:
	float m[16];
};

// 2차원 벡터
class Vector2
{
public:
	Vector2() : x(0.0f), y(0.0f) {}
	Vector2(float _x, float _y) : x(_x), y(_y) {}

	const float *GetPointer() const { return &x; }
	Vector2 &Set(float _x, float _y) { x = _x; y = _y; return *this; }

public:
	float x, y;
};

// 3차원 벡터
class Vector3
{
public:
	Vector3() : x(0.0f), y(0.0f), z(0.0f) {}
	Vector3(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {}

	Vector3 operator - () const { return Vector3(-x, -y, -z); }
	Vector3 operator + (const Vector3 &rhs) const { return Vector3(x + rhs.x, y + rhs.y, z + rhs.z); }
	Vector3 operator - (const Vector3 &rhs) const { return Vector3(x - rhs.x, y - rhs.y, z - rhs.z); }
	Vector3 operator * (const float &rhs) const { return Vector3(x * rhs, y * rhs, z * rhs); }
	Vector3 operator * (const Vector3 &rhs) const { return Vector3(x * rhs.x, y * rhs.y, z * rhs.z); }
	Vector3 operator / (const float &rhs) const { return Vector3(x / rhs, y / rhs, z / rhs); }
	Vector3 &operator += (const float &rhs) { x += rhs; y += rhs; z += rhs; return *this; }
	Vector3 &operator += (const Vector3 &rhs) { x += rhs.x; y += rhs.y; z += rhs.z; return *this; }
	Vector3 &operator *= (const float &rhs) { x *= rhs; y *= rhs; z *= rhs; return *this; }
	Vector3 &operator /= (const float &rhs) { x /= rhs; y /= rhs; z /= rhs; return *this; }

	friend Vector3 operator * (const float &lhs, const Vector3 &rhs) { return Vector3(lhs * rhs.x, lhs * rhs.y, lhs * rhs.z); }

	const float *GetPointer() const { return &x; }
	Vector3 &Set(float _x, float _y, float _z) { x = _x; y = _y; z = _z; return *this; }
	void MakeZero() { x = 0.0f; y = 0.0f; z = 0.0f; }
	float Length() const { return sqrtf(x * x + y * y + z * z); }
	Vector3 &Normalize() { float l = this->Length(); if(l != 0.0f) *this /= l; return *this; }
	float Dot(const Vector3 &rhs) const { return x * rhs.x + y * rhs.y + z * rhs.z; }
	Vector3 Cross(const Vector3 &rhs) const { return Vector3(y * rhs.z - z * rhs.y, z * rhs.x - x * rhs.z, x * rhs.y - y * rhs.x); }
#ifdef MISC_OPENGL
	Vector3 TransformNormal(const Matrix &rhs) const { return Vector3(x * rhs.m[0] + y * rhs.m[1] + z * rhs.m[2], x * rhs.m[4] + y * rhs.m[5] + z * rhs.m[6], x * rhs.m[8] + y * rhs.m[9] + z * rhs.m[10]); }
	Vector3 TransformCoord(const Matrix &rhs) const { return Vector3(x * rhs.m[0] + y * rhs.m[1] + z * rhs.m[2] + rhs.m[3], x * rhs.m[4] + y * rhs.m[5] + z * rhs.m[6] + rhs.m[7], x * rhs.m[8] + y * rhs.m[9] + z * rhs.m[10] + rhs.m[11]); }
#else
	Vector3 TransformNormal(const Matrix &rhs) const { return Vector3(x * rhs.m[0] + y * rhs.m[4] + z * rhs.m[8], x * rhs.m[1] + y * rhs.m[5] + z * rhs.m[9], x * rhs.m[2] + y * rhs.m[6] + z * rhs.m[10]); }
	Vector3 TransformCoord(const Matrix &rhs) const { return Vector3(x * rhs.m[0] + y * rhs.m[4] + z * rhs.m[8] + rhs.m[12], x * rhs.m[1] + y * rhs.m[5] + z * rhs.m[9] + rhs.m[13], x * rhs.m[2] + y * rhs.m[6] + z * rhs.m[10] + rhs.m[14]); }
#endif

public:
	float x, y, z;
};

// 4차원 벡터
class Vector4
{
public:
	Vector4() : x(0.0f), y(0.0f), z(0.0f), w(0.0f) {}
	Vector4(float _x, float _y, float _z, float _w) : x(_x), y(_y), z(_z), w(_w) {}

	const float *GetPointer() const { return &x; }
	Vector4 &Set(float _x, float _y, float _z, float _w) { x = _x; y = _y; z = _z; w = _w; return *this; }

public:
	float x, y, z, w;
};

// 광선
class Ray
{
public:
	Ray() {}
	Ray(const Vector3 &_start, const Vector3 &_end) : origin(_start), direction(_end - _start) { direction.Normalize(); }

	Vector3 Eval(float t) const { return origin + direction * t; }

public:
	Vector3 origin;	// 시작점
	Vector3 direction;	// 방향
};

// 불투명도와 색을 저장하는 구조체
struct Color
{
	Vector3 color;	// 색
	float opacity;	// 불투명도
};

// 볼륨 데이터를 나타내는 하나의 단위
struct Voxel
{
	unsigned char density;	// 밀도
	Vector3 normal;	// 법선 벡터(gradient vector)
	float opacity;	// 불투명도
};

class Material
{
public:
	Material() : ambient(0.2f, 0.2f, 0.2f), diffuse(0.5f, 0.5f, 0.5f), specular(0.5f, 0.5f, 0.5f) {}
	Material(const Vector3 &_ambient, const Vector3 &_diffuse, const Vector3 &_specular) : ambient(_ambient), diffuse(_diffuse), specular(_specular) {}

	Material &Set(const Vector3 &_ambient, const Vector3 &_diffuse, const Vector3 &_specular) { ambient = _ambient; diffuse = _diffuse; specular = _specular; return *this; }

public:
	Vector3 ambient;	// 자연광
	Vector3 diffuse;	// 확산광
	Vector3 specular;	// 반사광
};

// 방향성 광원
class DirectionalLight
{
public:
	DirectionalLight() {}
	DirectionalLight(const Vector3 &_direction, const Material &_material) : direction(_direction), material(_material) { direction.Normalize(); }

public:
	Vector3 direction;	// 방향
	Material material;	// 성질
};
