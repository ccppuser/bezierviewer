#pragma once

#include "Misc.h"

#define CAMERA_PERSPECTIVE 1
#define CAMERA_ORTHOGONAL 2

class ArcBallCamera
{
public:
	ArcBallCamera(float distanceFromTarget = 30.0f);
	virtual ~ArcBallCamera(void);

	Vector3 GetPosition();

	void SetCameraMode(int cameraMode);

	void FreezeRevolve(bool flag);

	/*
	 */
	void SetDistanceFromTarget(float distance);

	/*
	 */
	void SetProjection(int width, int height);

	/*
	 */
	void RevolveByAxisX(float angle);
	void RevolveByAxisY(float angle);
	void RevolveByAxisZ(float angle);

	void SetRevolveByAxisX(float angle);
	void SetRevolveByAxisY(float angle);
	void SetRevolveByAxisZ(float angle);

	/*
	 */
	void Update();

private:
	void UpdatePosition();

private:
	int m_nCameraMode;
	Vector3 m_ptPosition;
	Vector3 m_ptTarget;
	Vector3 m_vUp;
	float m_fDistanceFromTarget;
	float m_fAngleX;
	float m_fAngleY;
	float m_fAngleZ;
	bool m_bFreezeRevolve;
};

