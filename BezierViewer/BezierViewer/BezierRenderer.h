#pragma once

#include "OpenGL.h"

class BezierObject
{
public:
	BezierObject(RenderObject *pRObject)
		: m_pRObject(pRObject)
	{
	}
	virtual ~BezierObject()
	{
	}

	RenderObject *GetRenderObject()
	{
		return m_pRObject;
	}

	PCVertex *GetControlPoint(unsigned int index)
	{
		return (PCVertex *)m_pRObject->GetSubset(0)->m_vecVertex[index];
	}

	PCVertex *GetConvexHullPoint(unsigned int index)
	{
		return (PCVertex *)m_pRObject->GetSubset(1)->m_vecVertex[index];
	}

	virtual void Update() = 0;

protected:
	RenderObject *m_pRObject;
};

class BezierCurve : public BezierObject
{
public:
	BezierCurve(RenderObject *pRObject, int controlPointNumber)
		: BezierObject(pRObject)
		, m_nControlPointNumber(controlPointNumber)
		, m_nDegree(m_nControlPointNumber - 1)
	{
	}
	virtual ~BezierCurve()
	{
	}

	virtual void Update()
	{
		// 기존 베지어 곡선 제거
		m_pRObject->DeleteSubset(2);

		// 다시 계산
		m_pRObject->BeginNewSubset(GL_LINE_STRIP, VF_PC);
		for(float t = 0.0f; t <= 1.1f; t += 0.02f)	// 50번 정도 샘플링
		{
			if(t > 1.0f)
				break;

			m_pRObject->AddVertex(this->CaculateBezierPoint(t), Vector4(0.0f, 0.1f, 0.5f, 1.0f));
		}
		m_pRObject->EndNewSubset();
	}

private:
	Vector3 CaculateBezierPoint(float t)
	{
		Vector3 ret(0.0f, 0.0f, 0.0f);
		for(int i = 0; i < m_nControlPointNumber; ++i)
		{
			ret += this->GetBernsteinFunction(i, t);
		}
		return ret;
	}
	Vector3 GetBernsteinFunction(int controlPointNum, float t)
	{
		Vector3 controlPoint = this->GetControlPoint(controlPointNum)->position;
		float basis = GetFactorial(m_nDegree) / (GetFactorial(m_nDegree - controlPointNum) * GetFactorial(controlPointNum))
			* powf((1.0f - t), (float)(m_nDegree - controlPointNum))
			* powf(t, (float)controlPointNum);
		return basis * controlPoint;
	}
	int GetFactorial(int n)
	{
		int ret = 1;
		for(int i = n; i > 0; --i)
		{
			ret *= i;
		}
		return ret;
	}

private:
	int m_nControlPointNumber;
	int m_nDegree;
};

class BezierSurface : public BezierObject
{
public:
	BezierSurface(RenderObject *pRObject, int UDirControlPointNumber, int VDirControlPointNumber)
		: BezierObject(pRObject)
		, m_nUDirControlPointNumber(UDirControlPointNumber)
		, m_nVDirControlPointNumber(VDirControlPointNumber)
	{
	}
	virtual ~BezierSurface()
	{
	}

	virtual void Update()
	{
	}

private:
	int m_nUDirControlPointNumber;
	int m_nVDirControlPointNumber;
};

class BezierRenderer
{
public:
	BezierRenderer(void);
	~BezierRenderer(void);

	/* 베지어 렌더러 초기화
	 */
	bool Initialize(OpenGL *openGL);

	void Cleanup();

	BezierCurve *CreateBezierCurve(std::wstring name, int controlPointNumber);
	BezierSurface *CreateBezierSurface(std::wstring name, int UDirControlPointNumber, int VDirControlPointNumber);

	void PickControlPoint(CPoint windowPoint);

	void MoveControlPoint(CPoint windowPoint);

private:
	Ray CaculatePickingRay(CPoint windowPoint);
	float CaculateDistanceBetweenRayAndVertex(const Ray &ray, PCVertex *pVertex);

private:
	OpenGL *m_pOpenGL;
	std::vector<BezierObject *> m_vecBezierObject;
	BezierObject *m_pLastPickedObject;
	BezierObject *m_pCurrentPickedObject;
	unsigned int m_uPickedVertexIndex;
};

