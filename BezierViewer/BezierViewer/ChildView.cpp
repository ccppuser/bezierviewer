
// ChildView.cpp : implementation of the CChildView class
//

#include "stdafx.h"
#include "BezierViewer.h"
#include "ChildView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CChildView

CChildView::CChildView()
	: m_ptLastPoint(0, 0)
{
}

CChildView::~CChildView()
{
}


BEGIN_MESSAGE_MAP(CChildView, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_KEYDOWN()
	ON_WM_KILLFOCUS()
END_MESSAGE_MAP()



// CChildView message handlers

BOOL CChildView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return TRUE;
}

int CChildView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if(CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// OpenGL 초기화
	if(!theApp.m_pOpenGL->Initialize(this->m_hWnd))
	{
		TRACE0("OpenGL 초기화에 실패하였습니다.\n");
		return -1;
	}

	// BezierRenderer 초기화
	if(!theApp.m_pBezierRenderer->Initialize(theApp.m_pOpenGL))
	{
		TRACE0("BezierRenderer 초기화에 실패하였습니다.\n");
		return -1;
	}

	// OpenGL 렌더링 오브젝트 추가
	{
		// grid 추가
		RenderObject *pRObject = theApp.m_pOpenGL->CreateRenderObject(L"Grid");
		// grid 가로
		for(float z = -7.0f; z <= 7.0f; z += 1.0f)
		{
			pRObject->BeginNewSubset(GL_LINE_STRIP, VF_PC);
			if(z == 0.0f)
			{
				pRObject->AddVertex(-7.0f, 0.0f, z, 0.0f, 0.0f, 0.0f, 1.0f);
				pRObject->AddVertex(7.0f, 0.0f, z, 0.0f, 0.0f, 0.0f, 1.0f);
			}
			else
			{
				pRObject->AddVertex(-7.0f, 0.0f, z, 0.8f, 0.8f, 0.8f, 1.0f);
				pRObject->AddVertex(7.0f, 0.0f, z, 0.8f, 0.8f, 0.8f, 1.0f);
			}
			pRObject->EndNewSubset();
		}
		// grid 세로
		for(float x = -7.0f; x <= 7.0f; x += 1.0f)
		{
			pRObject->BeginNewSubset(GL_LINE_STRIP, VF_PC);
			if(x == 0.0f)
			{
				pRObject->AddVertex(x, 0.0f, -7.0f, 0.0f, 0.0f, 0.0f, 1.0f);
				pRObject->AddVertex(x, 0.0f, 7.0f, 0.0f, 0.0f, 0.0f, 1.0f);
			}
			else
			{
				pRObject->AddVertex(x, 0.0f, -7.0f, 0.8f, 0.8f, 0.8f, 1.0f);
				pRObject->AddVertex(x, 0.0f, 7.0f, 0.8f, 0.8f, 0.8f, 1.0f);
			}
			pRObject->EndNewSubset();
		}

		// 제어점 이동 좌표계 추가
		//pRObject = theApp.m_pOpenGL->CreateRenderObject(L"Coordinates");
	}

	// 카메라 위치 초기화
	ArcBallCamera *pCamera = theApp.m_pOpenGL->GetArcBallCamera();
	pCamera->RevolveByAxisX(3.14f * 0.25f);
	pCamera->RevolveByAxisY(3.14f * 0.25f);

	return 0;
}

void CChildView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	theApp.m_pOpenGL->GetArcBallCamera()->SetProjection(cx, cy);
}

void CChildView::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	
	// Do not call CWnd::OnPaint() for painting messages
}

void CChildView::OnLButtonDown(UINT nFlags, CPoint point)
{
	theApp.m_pBezierRenderer->PickControlPoint(point);
}

void CChildView::OnMouseMove(UINT nFlags, CPoint point)
{
	// 오른쪽 마우스 버튼 누른 채로 드래그하면 카메라 회전
	if(nFlags & MK_RBUTTON)
	{
		if(m_ptLastPoint.x == 0 && m_ptLastPoint.y == 0)
			m_ptLastPoint = point;

		CPoint delta(point - m_ptLastPoint);

		ArcBallCamera *pCamera = theApp.m_pOpenGL->GetArcBallCamera();
		pCamera->RevolveByAxisY((float)delta.x * theApp.m_fTimeDelta * 0.2f);
		pCamera->RevolveByAxisX((float)delta.y * theApp.m_fTimeDelta * 0.2f);
	}

	// 왼쪽 마우스 버튼 누른 채로 제어점 드래그하면 제어점 이동
	if(nFlags & MK_LBUTTON)
	{
		theApp.m_pBezierRenderer->MoveControlPoint(point);
	}

	m_ptLastPoint = point;
}

void CChildView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CWnd::OnKeyDown(nChar, nRepCnt, nFlags);
}

