#pragma once

#pragma comment(lib, "GLU32")
#pragma comment(lib, "GLAUX")
#pragma comment(lib, "OPENGL32")

#include <vector>
#include <algorithm>
#include "OpenGLMisc.h"
#include "ArcBallCamera.h"

#define SELECTBUFFERSIZE 256

class OpenGL
{
public:
	OpenGL();
	~OpenGL(void);

	/* OpenGL 환경 초기화
	 * 초기화 실패시 false리턴
	 */
	bool Initialize(HWND targetHandle);

	/*
	 */
	void Cleanup();

	/*
	 */
	void Render();

	ArcBallCamera *GetArcBallCamera();

	/*
	 */
	RenderObject *CreateRenderObject(std::wstring objectName);

	/*
	 */
	void DeleteRenderObject(std::wstring objectName);

	void SetTopView();
	void SetFrontView();
	void SetPerspectiveView();

	/* 윈도우 상 좌표로부터 3D 공간의 좌표를 구한다.
	 * 리턴되는 좌표는 y가 0.0f인 평면에서의 좌표이다.
	 */
	Vector3 PickZeroHeightCoord(int windowX, int windowY);

private:
	bool SetupPixelFormat(HDC hDC);
	void InitializeEnvironment(int width, int height);

private:
	HWND m_hWnd;
	HDC m_hDC;
	HGLRC m_hRC;
	ArcBallCamera m_camera;
	std::vector<RenderObject *> m_vecRObject;
	unsigned int m_nArrSelectBuf[SELECTBUFFERSIZE];
};

