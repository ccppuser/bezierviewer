#include "StdAfx.h"
#include "BezierRenderer.h"

#include <Float.h>


BezierRenderer::BezierRenderer()
	: m_pOpenGL(NULL)
	, m_pLastPickedObject(NULL)
	, m_pCurrentPickedObject(NULL)
	, m_uPickedVertexIndex(0xFFFFFFFF)
{
}
BezierRenderer::~BezierRenderer(void)
{
	this->Cleanup();
}

bool BezierRenderer::Initialize(OpenGL *openGL)
{
	if(!openGL)
		return false;

	m_pOpenGL = openGL;

	return true;
}

void BezierRenderer::Cleanup()
{
	for(std::vector<BezierObject *>::iterator itr = m_vecBezierObject.begin(); itr != m_vecBezierObject.end(); ++itr)
	{
		if((*itr))
		{
			delete (*itr);
			(*itr) = NULL;
		}
	}
	m_vecBezierObject.clear();
}

BezierCurve *BezierRenderer::CreateBezierCurve(std::wstring name, int controlPointNumber)
{
	// 베지어 곡선을 OpenGL 렌더링 목록에 추가 생성
	RenderObject *pRObject = m_pOpenGL->CreateRenderObject(name);

	// 제어점 생성
	pRObject->BeginNewSubset(GL_POINTS, VF_PC);
	for(int i = 0; i < controlPointNumber; ++i)
	{
		// x축을 따라 일정 간격으로 나열(청색)
		pRObject->AddVertex((float)i, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
	}
	pRObject->EndNewSubset();

	// 제어점 사이 연결(Convex Hull 표시)
	pRObject->BeginNewSubset(GL_LINE_STRIP, VF_PC);
	for(int i = 0; i < controlPointNumber; ++i)
	{
		// 제어점을 따라 직선으로 연결(녹색)
		pRObject->AddVertex((float)i, 5.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f);
	}
	pRObject->EndNewSubset();

	// 베지어 곡선 서브셋 준비
	pRObject->BeginNewSubset(GL_LINE_STRIP, VF_PC);
	pRObject->EndNewSubset();

	// 베지어 곡선 초기화
	BezierCurve *pNewBezierCurve = new BezierCurve(pRObject, controlPointNumber);
	pNewBezierCurve->Update();

	m_vecBezierObject.push_back(pNewBezierCurve);
	return pNewBezierCurve;
}

BezierSurface *BezierRenderer::CreateBezierSurface(std::wstring name, int UDirControlPointNumber, int VDirControlPointNumber)
{
	RenderObject *pRObject = m_pOpenGL->CreateRenderObject(name);
	BezierSurface *pNewBezierSurface = new BezierSurface(pRObject, UDirControlPointNumber, VDirControlPointNumber);
	m_vecBezierObject.push_back(pNewBezierSurface);
	return pNewBezierSurface;
}

void BezierRenderer::PickControlPoint(CPoint windowPoint)
{
	// 마우스로 윈도우를 클릭하여 3D 공간에 발사하는 광선 계산
	Ray ray = this->CaculatePickingRay(windowPoint);

	float fNearestDistance = FLT_MAX;
	BezierObject *pNearestObject = NULL;
	unsigned int uNearestIndex = 0xFFFFFFFF;

	// 모든 베지어 객체의 제어점과 피킹 광선 사이의 거리를 각각 계산
	for(std::vector<BezierObject *>::iterator itr = m_vecBezierObject.begin(); itr != m_vecBezierObject.end(); ++itr)
	{
		RenderObject::Subset *pSubset = (*itr)->GetRenderObject()->GetSubset(0);	// 제어점의 위치
		for(unsigned int i = 0; i < pSubset->m_vecVertex.size(); ++i)
		{
			PCVertex *pVertex = (PCVertex *)pSubset->m_vecVertex[i];

			// 점과 평면 사이 거리 구함
			float distanceFromRay = this->CaculateDistanceBetweenRayAndVertex(ray, pVertex);

			// 광선과 점의 최단 거리가 0.2f를 초과하면 피킹하지 않은 제어점으로 간주
			if(fabs(distanceFromRay) > 0.2f)
				continue;

			// 광선과 제일 가까운 제어점을 판별
			if(distanceFromRay < fNearestDistance)
			{
				fNearestDistance = distanceFromRay;
				pNearestObject = (*itr);
				uNearestIndex = i;
			}
		}
	}

	if(m_pLastPickedObject)
	{
		// 이전 제어점의 색을 흑색으로 돌려놓음
		PCVertex *pVertex = m_pLastPickedObject->GetControlPoint(m_uPickedVertexIndex);
		pVertex->color.Set(0.0f, 0.0f, 0.0f, 1.0f);

		m_pLastPickedObject = NULL;
	}

	// 최종 피킹한 제어점에 대한 처리
	if(pNearestObject)
	{
		m_pCurrentPickedObject = pNearestObject;
		m_uPickedVertexIndex = uNearestIndex;
		m_pLastPickedObject = m_pCurrentPickedObject;

		// 제어점의 색을 적색으로 바꿔줌
		PCVertex *pVertex = m_pCurrentPickedObject->GetControlPoint(m_uPickedVertexIndex);
		pVertex->color.Set(1.0f, 0.0f, 0.0f, 1.0f);
	}
}

void BezierRenderer::MoveControlPoint(CPoint windowPoint)
{
	Ray ray = this->CaculatePickingRay(windowPoint);

	// 해당 제어점을 포함하는 평면 상에서 제어점을 이동시킴
	// 평면은 viewport평면과 평행함

	// 평면의 방정식 구함
	Vector3 n = -ray.direction;
	Vector3 ptOnPlane = m_pCurrentPickedObject->GetControlPoint(m_uPickedVertexIndex)->position;
	float d = -(n.x * ptOnPlane.x + n.y * ptOnPlane.y + n.z * ptOnPlane.z);

	// ray와 평면의 교차점 계산
	Vector3 v = ray.direction;
	float t = -(n.Dot(ray.origin) + d) / (n.Dot(v));
	Vector3 ptIntersection = ray.Eval(t);

	// 제어점의 위치 변경
	m_pCurrentPickedObject->GetControlPoint(m_uPickedVertexIndex)->position = ptIntersection;
	m_pCurrentPickedObject->GetConvexHullPoint(m_uPickedVertexIndex)->position = ptIntersection;

	// 베지어 곡선 갱신
	m_pCurrentPickedObject->Update();
}

Ray BezierRenderer::CaculatePickingRay(CPoint windowPoint)
{
	Ray pickingRay;
	int viewport[4];
	double matProj[16];
	double matView[16];
	double wx, wy, wz;
	
	// 현재 변환 행렬 구함
	glGetIntegerv(GL_VIEWPORT, viewport);
	glGetDoublev(GL_MODELVIEW_MATRIX, matView);
	glGetDoublev(GL_PROJECTION_MATRIX, matProj);

	// 화면 안으로 발사하는 광선의 시작 점 구함
	gluUnProject((double)windowPoint.x, (double)(viewport[3] - windowPoint.y), 0.0, matView, matProj, viewport, &wx, &wy, &wz);
	pickingRay.origin.Set((float)wx, (float)wy, (float)wz);

	// 화면 안으로 발사하는 광선의 방향 구함
	gluUnProject((double)windowPoint.x, (double)(viewport[3] - windowPoint.y), 0.1, matView, matProj, viewport, &wx, &wy, &wz);
	pickingRay.direction = (Vector3((float)wx, (float)wy, (float)wz) - pickingRay.origin).Normalize();

	return pickingRay;
}

float BezierRenderer::CaculateDistanceBetweenRayAndVertex(const Ray &ray, PCVertex *pVertex)
{
	// 피킹 광선과 점의 최단 거리를 구하여 피킹 대상에 해당하는 제어점인지를 파악

	// 평면의 방정식 도출
	Vector3 pt1 = ray.origin;	// 평면 위의 첫 번째 점
	Vector3 pt2 = ray.Eval(1.0f);	// 평면 위의 두 번째 점
	Vector3 v1 = ray.direction;
	Vector3 v2 = (pVertex->position - ray.origin).Normalize();
	Vector3 v3 = v1.Cross(v2).Normalize();

	Vector3 n = v3.Cross(v1).Normalize();
	float d = -(n.x * pt1.x + n.y * pt1.y + n.z * pt1.z);

	// 점과 평면 사이 거리 구함
	Vector3 pt = pVertex->position;
	return n.x * pt.x + n.y * pt.y + n.z * pt.z + d;
}
