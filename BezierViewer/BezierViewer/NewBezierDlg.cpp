// NewCurveDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BezierViewer.h"
#include "NewBezierDlg.h"
#include "afxdialogex.h"


// CNewBezierCurveDlg dialog

IMPLEMENT_DYNAMIC(CNewBezierCurveDlg, CDialog)

CNewBezierCurveDlg::CNewBezierCurveDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNewBezierCurveDlg::IDD, pParent)
{

}

CNewBezierCurveDlg::~CNewBezierCurveDlg()
{
}

void CNewBezierCurveDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_EDT_CURVENAME, m_edtName);
	DDX_Control(pDX, IDC_EDT_CONTROLPOINTNUMBER, m_edtControlPointNumber);
}

void CNewBezierCurveDlg::OnOK()
{
	// 에디트 박스로부터 객체의 이름을 얻음
	m_edtName.GetWindowTextW(m_strName);

	// 에디트 박스로부터 베지어 곡선의 제어점 개수를 얻음
	CString strTemp;
	m_edtControlPointNumber.GetWindowTextW(strTemp);
	m_nControlPointNumber = _wtoi(strTemp.GetBuffer());
	if(m_nControlPointNumber < 3)
	{
		MessageBox(L"The number of control points must bigger than 2.", L"Exception", MB_OK);
		return;
	}

	CDialog::OnOK();
}


BEGIN_MESSAGE_MAP(CNewBezierCurveDlg, CDialog)
END_MESSAGE_MAP()


// CNewBezierCurveDlg message handlers


// CNewBezierSurfaceDlg dialog

IMPLEMENT_DYNAMIC(CNewBezierSurfaceDlg, CDialog)

CNewBezierSurfaceDlg::CNewBezierSurfaceDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNewBezierSurfaceDlg::IDD, pParent)
{

}

CNewBezierSurfaceDlg::~CNewBezierSurfaceDlg()
{
}

void CNewBezierSurfaceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_EDT_SURFACENAME, m_edtName);
	DDX_Control(pDX, IDC_EDT_U_CONTROLPOINTNUMBER, m_edtUDirControlPointNumber);
	DDX_Control(pDX, IDC_EDT_V_CONTROLPOINTNUMBER, m_edtVDirControlPointNumber);
}

void CNewBezierSurfaceDlg::OnOK()
{
	// 에디트 박스로부터 객체의 이름을 얻음
	m_edtName.GetWindowTextW(m_strName);

	// 에디트 박스로부터 베지어 곡면의 각 방향별 제어점 개수를 얻음
	CString strTemp;
	m_edtUDirControlPointNumber.GetWindowTextW(strTemp);
	m_nUDirControlPointNumber = _wtoi(strTemp.GetBuffer());
	if(m_nUDirControlPointNumber < 3)
	{
		MessageBox(L"The number of control points must bigger than 2.", L"Exception", MB_OK);
		return;
	}

	m_edtVDirControlPointNumber.GetWindowTextW(strTemp);
	m_nVDirControlPointNumber = _wtoi(strTemp.GetBuffer());
	if(m_nVDirControlPointNumber < 3)
	{
		MessageBox(L"The number of control points must bigger than 2.", L"Exception", MB_OK);
		return;
	}

	CDialog::OnOK();
}


BEGIN_MESSAGE_MAP(CNewBezierSurfaceDlg, CDialog)
END_MESSAGE_MAP()


// CNewCurveDlg message handlers
