//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by BezierViewer.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_BezierViewerTYPE            130
#define IDD_NEWBEZIERCURVEDLG           310
#define IDD_NEWBEZIERSURFACEDLG         311
#define IDC_EDT_CONTROLPOINTNUMBER      1000
#define IDC_EDT_V_CONTROLPOINTNUMBER    1001
#define IDC_EDT_U_CONTROLPOINTNUMBER    1002
#define IDC_EDT_SURFACENAME             1003
#define IDC_EDT_CURVENAME               1004
#define ID_VIEW_CAMERA                  32771
#define ID_CAMERA_TOP                   32772
#define ID_CAMERA_FRONT                 32773
#define ID_CAMERA_PERSPECTIVE           32774
#define ID_Menu                         32779
#define ID_EDIT_NEW                     32780
#define ID_NEW_BEZIERCURVE              32781
#define ID_NEW_BEZIERSURFACE            32782

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        312
#define _APS_NEXT_COMMAND_VALUE         32787
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           311
#endif
#endif
